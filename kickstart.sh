#!/bin/bash
#
# @file server
# @brief Bash script to automatically setup an IoT server.

# @description Call debian-base ansible role to setup 
# the server (see https://github.com/constrict0r/debian-kickstart).
#
# @arg $1 string Username to be created and added to sudoers, example: 'huertico'.
# @arg $2 string Password to be assigned to the new user, example: 'huertico'.
#
# @exitcode 0 If successful.
# @exitcode 1 On failure.
function main() {

    local KICKSTART_URL='https://raw.githubusercontent.com/constrict0r/debian-kickstart/master/kickstart.sh'
    local GIT_URL='https://gitlab.com/huertico/server'
    wget https://gitlab.com/huertico/server/raw/master/files/packages.yml -O packages.yml
    local PACKAGE_FILE_PATH=$(readlink -f packages.yml)

    local USERNAME='huertico'
    local PASSWORD='huertico'

    bash <(curl -sL "$KICKSTART_URL") "-u $USERNAME " "-w $PASSWORD " "-g $GIT_URL " "-p $PACKAGE_FILE_PATH " "$@"

#    # Install node-red.
#    curl -sL https://deb.nodesource.com/setup_12.x | bash -
#    apt-get install -y nodejs
#    npm install -g --unsafe-perm node-red
#    mkdir /root/.node-red &>/dev/null

#    ansible localhost -m include_role -a name=constrict0r.debian_system_configuration --extra-vars "configuration_git_url=$GIT_URL"

#    # Enable graphite.
#    ln -s /etc/uwsgi/apps-available/graphite-api.ini /etc/uwsgi/apps-enabled
#    ln -s /etc/nginx/sites-available/graphite.conf /etc/nginx/sites-enabled

#    # Set mosquitto password.
#    mosquitto_passwd -b /etc/mosquitto/pw huertico huertico

#    # Remove nginx default.
#    rm -r /etc/nginx/default &>/dev/null

#    # Enable services.
#    systemctl daemon-reload
#    systemctl enable carbon-cache
#    systemctl enable mosquitto
#    systemctl enable nginx
#    systemctl enable nodered.service
#    systemctl enable uwsgi
#    systemctl enable ssh
#    systemctl restart nodered.service
#    systemctl restart uwsgi
#    systemctl restart ssh

#    apt-get autoremove -y

    # Correct Raspbian issue:
    # "Cannot open access to console, the root account is locked. See sulogin(8) man page. Press Enter to continue.".
    [[ -f /sbin/sulogin ]] && mv /sbin/sulogin /sbin/sulogin.bk

    return 0
}

# Avoid running the main function if we are sourcing this file.
return 0 2>/dev/null
main "$@"
