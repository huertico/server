server
======

![IoT server](https://gitlab.com/huertico/repo-images/raw/master/wind.png)

![build](https://gitlab.com/huertico/server/badges/master/build.svg)

Open Hardware/Software for automated plant growing, server setup.

This repository setups an [Internet of Things](https://en.wikipedia.org/wiki/Internet_of_Things) (*IoT*) server by runnning the [debian-base](https://github.com/constrict0r/debian-base) [Ansible](https://www.ansible.com) role and copying the configuration layout contained on this repository to the [root directory](https://unix.stackexchange.com/questions/297969/is-the-slash-part-of-the-name-of-the-linux-root-directory) of the system.

The resulting system is a server who is responsible for read humidity, temperature and light sensors values, and that instructs water sources to be opened when some threshold limit is reached (see [infrastructure](#infrastructure)).

Ingredients
-----------

[![ansible](https://gitlab.com/huertico/repo-images/raw/master/ansible.png)](https://www.ansible.com)
[![grafana](https://gitlab.com/huertico/repo-images/raw/master/grafana.png)](https://grafana.com/)
[![graphite](https://gitlab.com/huertico/repo-images/raw/master/graphite.png)](https://graphiteapp.org/)
[![mosquitto](https://gitlab.com/huertico/repo-images/raw/master/mosquitto.png)](https://nginx.org/)
[![nginx](https://gitlab.com/huertico/repo-images/raw/master/nginx.png)](https://nginx.org/)
[![nodered](https://gitlab.com/huertico/repo-images/raw/master/nodered.png)](https://nodered.org/)

Usage
-----

To setup the server, **become the** [root user](https://en.wikipedia.org/wiki/Superuser) and then run:

```
    bash <(curl -sL https://gitlab.com/huertico/server/raw/master/kickstart.sh)
```

The username and password used on all included services for this server are: *huertico:huertico* if you want to change it run:

```
    bash <(curl -sL https://gitlab.com/huertico/server/raw/master/kickstart.sh)-u myUser -w myPassword
```

To use this server with [Docker](https://www.docker.com/):

```
    docker pull registry.gitlab.com/huertico/server
```

To use this server as an image for a [qemu](https://www.qemu.org/) virtual machine or to put on a [SD card](https://en.wikipedia.org/wiki/Secure_Digital) for a [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/), see [server-image](https://gitlab.com/huertico/server-image).

Infrastructure
---------------

The basic system infrastructure consist of three separated hardware units and a stack of services used to support communication, control and data storage between units.

The units use wifi to send and receive data, the general system implementation is shown in the next figure:

![deployment](https://gitlab.com/huertico/repo-images/raw/master/server/deployment.png)

The system units are:

- A [watcher unit](https://gitlab.com/huertico/watcher) composed by sensors, collects data about temperature, humidity and light conditions and publish that data to a [MQTT](http://mqtt.org/) queue.

- A **server unit** reads the data from the [MQTT](http://mqtt.org/) queue and determines if is necesary to open a water source to water the plants, this unit is also responsible for store the data on a Time Series Database.

- A [collector unit](https://gitlab.com/huertico/watering) composed by actuators (i.e.: relays, solenoids), is responsible for opening and closing water sources when the server unit demands it.

The control component used for the **watcher** and the **collector** units are [ESP8266](http://esp8266.net/) or [ESP32](http://esp32.net/) microcontrollers and the server used is a [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero/).

The main communication channel between system parts is the **MQTT** messages queue, this service works together with other services to conform the **services stack**, the services stack consist of the following services:

- [Mosquitto](https://mosquitto.org/) [MQTT](http://mqtt.org/) messages queue.
- [Nginx](https://nginx.org/) web server.
- [Node-RED](https://nodered.org/) IoT platform.
- [Grafana](https://grafana.com/) analytics tool.
- [Graphite](https://graphiteapp.org/) Time Series Database.
- [Sqlite](https://sqlite.org/index.html) backend database for Graphite.
- [SSH](https://www.openssh.com/) for remote access.
- [Ansible](https://www.ansible.com) configuration management tool.

The next figure shows the interactions on the system:

![deployment](https://gitlab.com/huertico/repo-images/raw/master/server/sequence.png)

A Node-RED instance on the server reads the sensors data through the MQTT queue and when a certain threshold limit is reached, publish an **act** command on the MQTT channel that belongs to the **collector**, when the **collector** reads the message from this channel knows that is necessary to open a water source.

All services included on this server are runned on their default ports.

Mosquitto (MQTT)
----------------

[Mosquitto](https://mosquitto.org/) is a lightweight MQTT broker.

On this system Mosquitto is used as the main communication channel between system units.

When the service is running, you can subscribe and publish to the Mosquitto broker, for example:

To publish as client c1 on **data** channel:

```
    mosquitto_pub -d -i c1 -t 'huertico/c1/data'  -p 1883 \
      -m '{"id":"c1","type":"earth","data":{"light":956,"moisture":750,"humidity":39,"temperature":20}}'
```

To subscribe as the client c2 to the client c2 **act** channel:

```
    mosquitto_sub -i c2 -t 'huertico/c2/act' -p 1883
```

To publish as the user huertico to the client c2 **act** channel indicating that component c4 must be used:

```
    mosquitto_pub -d -u huertico -P huertico -t 'huertico/c2/act' -m '{"id": c4}'
```

Here Mosquitto is configured to:

- Allow a watcher to plublish only on a **/data** channel with its client id.

- Allow a collector to subscribe only to a **/act** channel with its client id.

- Allow the server to use a *huertico* user to subscribe to all **/data** channels and to write to all **/act** channels.

The full list of Mosquitto topics is:

- huertico/%c/join: Allow units to be included in the system by client id.

- huertico/%c/unjoin: Allow units to be excluded from the system by client id.

- huertico/%c/update: Allow units to update its information by client id.

- huertico/%c/data : Allows watchers to publish data by client id.

- huertico/%c/act : Allows collectors to subscribe and read data by client id.

- huertico/associate : Allows two units to be associated by a [sequence of components](https://gitlab.com/huertico/server).

- huertico/disassociate: Allows two units to be disassociated.

Nginx
-----

[Nginx](https://nginx.org/) is a fast and popular web server.

On this system nginx is used as a [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy) for Graphite to expose its data.

To see Graphite data through nginx go to [localhost](https://localhost) url on a web browser.

Node-RED
--------

[Node-RED](https://nodered.org/) is a platform for the Internet of Things written on javascript.

On this system Node-RED is used to:

- Monitoring the sensors data published on Mosquitto by the watcher unit.

- Verify if a threshold is reached and if it is, publish an **act** command to the collector unit channel on Mosquitto.

- Tell Graphite to save the sensors or actuators data.

The data between nodes is transmitted using [json](https://json.org/) format:

![Node-RED-flow](https://gitlab.com/huertico/repo-images/raw/master/server/node-red-flow.png)

To use the Node-RED interface go to [localhost:1880](https://localhost:1880) url on a web browser.

Grafana
-------

[Grafana](https://grafana.com/) is an analytics tool for Time Series Databases.

On this system Grafana is used to show pretty analytics from the data stored by Graphite.

Graphite
--------

[Graphite](https://graphiteapp.org/) is a Time Series Databases optimized to store high volume data over periods of time.

On this system Graphite is used to store the sensors and actuators data.

The Graphite subsystem is composed by multiple components:

![graphite](https://gitlab.com/huertico/repo-images/raw/master/server/graphite.png)

Nginx receives [http](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) requests on port 8081 and forwards them to the [uwsgi](https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html) application server, uwsgi then forwards the request to the [graphite-api](https://graphite-api.readthedocs.io/en/latest/) application who is responsible to fetch metrics from the [graphite-carbon](https://graphiteapp.org/) [Time Series Database](https://en.wikipedia.org/wiki/Time_series_database), likewise graphite-carbon uses sqlite as backend to stores its data.

To see Graphite data go to [localhost](https://localhost) url on a web browser.

Sqlite
------

[Sqlite](https://sqlite.org/index.html) is a lightweight database system that uses a file to store its data.

On this system sqlite is used as the backend database for Graphite.

SSH
---

[SSH](https://www.openssh.com/) or secure shell hosting is a protocol to gain remote access to a computer system.

On this system ssh is used to provide remote access to the server.

To access remotely to the server unit run on a terminal:

```
    ssh huertico@server-ip-address
```

Ansible
-------

[Ansible](https://www.ansible.com) is a configuration management tool that allows to automatize system tasks.

On this system Ansible is used to setup the server and then is uninstalled.

A collection of Ansible roles configures a basic Debian system and on top of it installs and configures the full stack of services, the general setup process is shown in the following activity diagram:

![main](https://gitlab.com/huertico/repo-images/raw/master/server/main.png)

A git repository URL is used to download configuration to be copied to the root directory of the filesystem as a simple way to apply system-wide configuration.

The stack of [Ansible roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) used is shown below:

![deployment](https://gitlab.com/huertico/repo-images/raw/master/server/roles-deployment.png)

For more information on this roles stack see [debian-kickstart](https://github.com/constrict0r/debian-kickstart), [debian-base](https://github.com/constrict0r/debian-base) and [debian-system-configuration](https://github.com/constrict0r/debian-system-configuration).

Requirements
------------

The command line tool [curl](https://curl.haxx.se/) must be installed.

Compatibility
-------------

- [Debian buster](https://wiki.debian.org/DebianBuster).
- [Debian stretch](https://wiki.debian.org/DebianStretch).
- [Raspbian stretch](https://raspbian.org/).

License
-------

GPL 3. See the [LICENSE](https://gitlab.com/huertico/server/raw/master/LICENSE) file for more details.

Author Information
------------------

This repository was created by [![constrict0r](https://gitlab.com/huertico/repo-images/raw/master/constrict0r.png)](https://geekl0g.wordpress.com/author/constrict0r) and [![valarauco](https://gitlab.com/huertico/repo-images/raw/master/valarauco.png)](https://twitter.com/valarauco).

Enjoy!!

![enjoy](https://gitlab.com/huertico/repo-images/raw/master/huertico.png)