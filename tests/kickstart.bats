#!/usr/bin/env bats
#
# Kickstart script tests.
# This test must be runned as root.
# This test must be runned from the root repository directory.

@test "Run kickstart." {
    run ./kickstart.sh
    [[ -d /home/huertico ]]
    [[ -f /usr/sbin/nginx ]]
 #   [[ -f /etc/nginx/sites-available/graphite.conf ]]
}
