#!/bin/sh

set -e

if [ -n "$MOSQUITTO_USER" -a -n "$MOSQUITTO_PASSWD" ]; then 
  echo "Setting new MQTT user ($MOSQUITTO_USER) and password."
  mosquitto_passwd -b /etc/mosquitto/pw $MOSQUITTO_USER $MOSQUITTO_PASSWD
  sed -i "s/user .*/user $MOSQUITTO_USER/" /etc/mosquitto/acl
fi

exec "$@"
